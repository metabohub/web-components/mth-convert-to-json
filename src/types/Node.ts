export interface Node {
  label: string;
  metadata: {[key: string]: string | number | {[key: string]: string | number} | Array<string>};
}