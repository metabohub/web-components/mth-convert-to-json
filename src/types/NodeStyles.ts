export interface NodeStyles {
  [key: string]: {[key: string]: string | number};
}