export interface Link {
  source: string;
  target: string;
  classes?: Array<string>;
  metadata?: {[key: string]: string | number | {[key: string]: string | number} | Array<string | number>};
}