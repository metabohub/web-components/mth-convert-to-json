export interface LinkStyles {
  [key: string]: {[key: string]: string | number};
}