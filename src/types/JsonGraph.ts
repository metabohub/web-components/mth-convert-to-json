import type { Link } from "./Link";
import type { Node } from "./Node";
import type { MetaData } from "./MetaData";

export interface JsonGraph {
	graph: {
		id: string;
		type: string;
		directed: boolean;
		nodes: {[key: string]: Node};
		edges: Array<Link>;
		metadata: MetaData;
	}
}