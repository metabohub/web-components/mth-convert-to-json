import { LinkStyles } from "./LinkStyles";
import { NodeStyles } from "./NodeStyles";

export interface MetaData {
  style?: {[key: string]: NodeStyles | LinkStyles};
}