import type { Node } from "./types/Node";
import type { Link } from "./types/Link";
import type { JsonGraph } from "./types/JsonGraph";

/**
 * A - Anatomy : #0974C8
 * B - Organism : #0ADCE7
 * C - Diseases : #BD09C8
 * D - Chemical and drugs : #C85D09
 * E - Analytical, Diagnostic and Therapeutic Techniques and Equipment : #E7150A
 * F - Psychiatry and Psychology : #840AE7
 * G - Phenomena and Processes : #15C809
 * H - Disciplines and Occupations : #6EE70A
 * I - Anthropology, Education, Sociology and Social Phenomena : #007115
 * J - Technology and Food and Beverages : #00714E
 * K - Humanities : #005C71
 * L - Information science : #002471
 * M - Persons : #150071
 * N - Health Care : #4D0071
 * V - Publication characteristics : #710026
 * Z - Geography locations : #000000
 */

const categoriesColorPanel: {[key: string]: string} = {
  A: '#0974C8',
  B: '#0ADCE7',
  C: '#BD09C8',
  D: '#C85D09',
  E: '#E7150A',
  F: '#840AE7',
  G: '#15C809',
  H: '#6EE70A',
  I: '#007115',
  J: '#00714E',
  K: '#005C71',
  L: '#002471',
  M: '#150071',
  N: '#4D0071',
  V: '#710026',
  Z: '#000000'
};

/**
 * Take as input a JSON string and remove all undefined without quote values
 * @param jsonString json to process
 * @returns JSON String preprocess
 */
function jsonValidator(jsonString: string): string {
  return jsonString.replace(/undefined/g, 'null');
}

/**
 * Return specific color according to MeSh category
 * @param category MeSH term category
 * @returns String of Hex color
 */
function returnCategoryColor(category: string): string {
  if (Object.keys(categoriesColorPanel).includes(category)) {
    return categoriesColorPanel[category];
  } else {
    return '#000000';
  }
}

/**
 * Convert JSON of compound graph from Neo4J to compound graph at JSON Graph format
 * @param compoundGraph JSON of compound graph
 * @returns JSON string of compound graph at JSON Graph format
 */
export function CompoundGraphToJSONGraph(compoundGraph: string): string {
  const dataString = jsonValidator(compoundGraph);
  const dataJSON = JSON.parse(dataString);

  // Mesh which will be put on the left
	let leftMeshes:Array<string>=[];
	// Mesh which will be put on the right
	let rightMeshes:Array<string>=[];
	// Width from compounds to mesh
	const visWidth= 500;
	// Vertical padding between nodes
	const verticalPadding=30;
  // Vertical order for compound
  let currentYMetabolite = 0;

  let categoryhMap:{[key:string]:Array<Node>}={};
	let categorySizeMap:{[key:string]:number}={};

  // Define global graph data
  const jsonGraph: JsonGraph = {
    graph: {
      id: 'compoundGraph',
      type: 'compound',
      directed: false,
      nodes: {},
      edges: [],
      metadata: {
        style: {
          nodeStyles : {},
          linkStyles: {}
        }
      }
    }
  };

  // Define global style
  const compoundStyle = {
    width: 20,
    height: 20,
    strokeWidth: 1,
    shape: 'circle'
  };

  const conceptStyle = {
    width: 20,
    height: 20,
    strokeWidth: 1,
    shape: 'rect'
  }

  const classicEdge = {
    strokeWidth: 1,
		opacity: 0
  }

  if (jsonGraph.graph.metadata.style && jsonGraph.graph.metadata.style.nodeStyles) {
    jsonGraph.graph.metadata.style.nodeStyles = {
      concept: conceptStyle,
      compound: compoundStyle
    }
  }

  if (jsonGraph.graph.metadata.style && jsonGraph.graph.metadata.style.linkStyles) {
    jsonGraph.graph.metadata.style.linkStyles = {
      classicEdge: classicEdge
    }
  }

  // Create nodes object
  dataJSON.nodes.forEach((compound: {[key:string]: string}) => {
    const node: Node = {
      label: '',
      metadata: {}
    }

    const id = compound.id;
    node.label = compound.name;

    if (compound.type === 'metabolite') {
      currentYMetabolite += 1;
      const nodeY = currentYMetabolite * verticalPadding + 1;
      node.metadata = {
        classes: ['compound'],
        position: {
          x: 1,
          y: nodeY
        }
      }
      if (compound.centrality) {
        node.metadata.centrality = compound.centrality;
      }
    }

    if (compound.type === 'concept') {
      node.metadata = {
				meSHParentTreeNb: compound.meSHParentTreeNb,
				meSHTreeNb: compound.meSHTreeNb,
				category: compound.category,
        classes: ['concept', compound.category],
        position: {
          x: 0,
          y: 0
        }
			}
      if(categoryhMap[compound.category]){
				categoryhMap[compound.category].push(node);
			}
			else{
				categoryhMap[compound.category]=[];
				categoryhMap[compound.category].push(node);
			}
      if (compound.centrality) {
        node.metadata.centrality = compound.centrality;
      }
    }

    jsonGraph.graph.nodes[id] = node;
  });

  // Sort Categories
  Object.keys(categoryhMap).forEach((cat:string)=>{
		let value=categoryhMap[cat].length;
		categorySizeMap[cat]=value;
	});

	let setSize:Array<Array<string | number>>=[];
	Object.keys(categorySizeMap).forEach((cat:string)=>{
		let tmpArray=[cat,categorySizeMap[cat]];
		setSize.push(tmpArray);
	});
	setSize.sort((a:any, b:any) => b[1] - a[1] );

	var leftCount=0;
	var rightCount=0;
	let leftCategories:Array<string>=[];
	var rightCategories:Array<string>=[];

	setSize.forEach((list:Array<string | number>)=>{
		let cat=list[0] as string;
		let val=list[1] as number;
		if(leftCount===0)
			{
				leftCategories.push(cat);
				leftCount+=val;
			}
			else{
				if(leftCount<rightCount){
					leftCategories.push(cat);
					leftCount+=val;
				}
				else{
					rightCategories.push(cat);
					rightCount+=val;
				}
			}	
	});

  let startingYMeshPoints:{[key:string]:number}={};
	let leftY=0;
	let rightY=0;
	leftCategories.forEach((cat:string)=>{
		let val=categorySizeMap[cat];
		startingYMeshPoints[cat]=leftY;
		leftY+=val;
	});
	rightCategories.forEach((cat:string)=>{
		let val=categorySizeMap[cat];
		startingYMeshPoints[cat]=rightY;
		rightY+=val;
	});

  Object.keys(jsonGraph.graph.nodes).forEach((nodeID: string) => {
    const nodeClass = jsonGraph.graph.nodes[nodeID].metadata.classes as Array<string>;
    if (nodeClass.includes("concept")) {
      let category="NaN";
      //get the category of the node
      if(jsonGraph.graph.nodes[nodeID].metadata)
        if(jsonGraph.graph.nodes[nodeID].metadata?.category)
          category = jsonGraph.graph.nodes[nodeID].metadata?.category as string;

      if(rightCategories.includes(category)){
        startingYMeshPoints[category]++;
        leftMeshes.push(nodeID);
        const nodePos = {
          x: visWidth,
          y: (startingYMeshPoints[category]) * verticalPadding
        }
        jsonGraph.graph.nodes[nodeID].metadata.position = nodePos;
      } else {
        startingYMeshPoints[category]++;
        rightMeshes.push(nodeID);
        const nodePos = {
          x: -visWidth,
          y: (startingYMeshPoints[category]) * verticalPadding
        }
        jsonGraph.graph.nodes[nodeID].metadata.position = nodePos;
      }
    }
  });

  // Create edges object
  dataJSON.edges.forEach((edge: {[key:string]: string}) => {
    let targetNodeClass = jsonGraph.graph.nodes[edge.target].metadata.classes as Array<string>;
    let sourceNodeClass = jsonGraph.graph.nodes[edge.source].metadata.classes as Array<string>;
    let category = "";

    if (targetNodeClass.includes('concept')) {
      category = jsonGraph.graph.nodes[edge.target].metadata.category as string;
    }

    if (sourceNodeClass.includes('concept')) {
      category = jsonGraph.graph.nodes[edge.source].metadata.category as string;
    }

    const link: Link = {
      source: edge.source,
      target: edge.target,
      classes: ['classicEdge', category],
      metadata: {}
    }

    jsonGraph.graph.edges.push(link);
  });

  // Create styles classes for categories
  Object.keys(categoryhMap).forEach((category: string) => {
    jsonGraph.graph.metadata.style!.nodeStyles[category] = {
      fill: returnCategoryColor(category)
    }
    jsonGraph.graph.metadata.style!.linkStyles[category] = {
      stroke: returnCategoryColor(category)
    }
  });

  return JSON.stringify(jsonGraph);
}

export function JGraphToJSONGraph(graphString: string): string {
  const dataString = jsonValidator(graphString);
  const dataJSON = JSON.parse(dataString);

  // Define global graph data
  const jsonGraph: JsonGraph = {
    graph: {
      id: 'compoundGraph',
      type: 'compound',
      directed: false,
      nodes: {},
      edges: [],
      metadata: {
        style: {
          nodeStyles : {},
          linkStyles: {}
        }
      }
    }
  };

  // Define global style
  const compoundStyle = {
    width: 20,
    height: 20,
    strokeWidth: 1,
    shape: 'circle'
  };

  const conceptStyle = {
    width: 20,
    height: 20,
    strokeWidth: 1,
    shape: 'rect'
  }

  const classicEdge = {
    strokeWidth: 1,
		opacity: 0
  }

  if (jsonGraph.graph.metadata.style && jsonGraph.graph.metadata.style.nodeStyles) {
    jsonGraph.graph.metadata.style.nodeStyles = {
      concept: conceptStyle,
      compound: compoundStyle
    }
  }

  if (jsonGraph.graph.metadata.style && jsonGraph.graph.metadata.style.linkStyles) {
    jsonGraph.graph.metadata.style.linkStyles = {
      classicEdge: classicEdge
    }
  }

  // Create nodes object
  dataJSON.nodes.forEach((compound: {[key:string]: string}) => {
    const node: Node = {
      label: '',
      metadata: {}
    }

    const id = compound.id;
    node.label = compound.name;

    if (compound.type === 'metabolite') {
      node.metadata = {
        classes: ['compound'],
        position: {
          x: 1,
          y: 1
        }
      }
    }

    if (compound.type === 'concept') {
      node.metadata = {
				broaderDescriptor: compound.broaderDescriptor,
        classes: ['concept'],
        position: {
          x: 0,
          y: 0
        }
			}
    }

    jsonGraph.graph.nodes[id] = node;
  });

  // Create edges object
  dataJSON.edges.forEach((edge: {[key:string]: string}) => {

    const link: Link = {
      source: edge.source,
      target: edge.target,
      classes: ['classicEdge'],
      metadata: {
        label: edge.label
      }
    }

    jsonGraph.graph.edges.push(link);
  });

  return JSON.stringify(jsonGraph);
}



/**
 * Convert json of graph from MetExploreViz to json of graph at JSON Graph format
 * @param metexplorevizGraph JSON of graph from MetExploreViz
 * @returns JSON string of graph at JSON Graph format
 */
export function MetExploreVizJSONToJSONGraph(metexplorevizGraph: string): string {
  const dataString = jsonValidator(metexplorevizGraph);
  const dataJSON = JSON.parse(dataString);

  const d3Nodes = dataJSON.sessions.viz.d3Data.nodes;
  const d3Links = dataJSON.sessions.viz.d3Data.links;
  let networkId = '';

  if (dataJSON.sessions.viz.id) {
    networkId = dataJSON.sessions.viz.id;
  } else {
    networkId = 'MetExploreViz Graph';
  }

  const jsonGraph: JsonGraph = {
    graph: {
      id: networkId,
      type: 'metabolic',
      directed: true,
      nodes: {},
      edges: [],
      metadata: {
        style: {}
      }
    }
  };

  const bindIndexDBID: {[key: number]: string} = {};

  d3Nodes.forEach((n: {[key: string]: string | number}, index: number) => {
    if (n.biologicalType === 'metabolite' || n.biologicalType === 'reaction') {
      const node: Node = {
        label: '',
        metadata: {
          classes: [n.biologicalType]
        }
      }

      const id = n.dbIdentifier;
      bindIndexDBID[index] = id as string;

      if (!n.name) {
        node.label = n.dbIdentifier as string;
      } else {
        node.label = n.name as string;
      }

      const position = {
        x: 0,
        y: 0
      };

      if (n.x) {
        position.x = n.x as number;
      }

      if (n.y) {
        position.y = n.y as number;
      }

      if (n.pathways) {
        node.metadata['pathways'] = n.pathways;
      }

      if (n.compartment) {
        node.metadata['compartment'] = n.compartment;
      }

      if (n.isSideCompound) {
        node.metadata['sideCompound'] = n.isSideCompound;
      }

      if (n.reactionReversibility) {
        node.metadata['reversible'] = n.reactionReversibility;
      }

      node.metadata['position'] = position;

      jsonGraph.graph.nodes[id] = node;
    }
  });

  d3Links.forEach((edge: {[key: string]: string | number}) => {
    const sourceNode = bindIndexDBID[edge.source as number];
    const targetNode = bindIndexDBID[edge.target as number];

    if (sourceNode && targetNode) {
      const link: Link = {
        source: bindIndexDBID[edge.source as number],
        target: bindIndexDBID[edge.target as number],
        classes: ['classicEdge'],
        metadata: {
          reversible: edge.reversible
        }
      }
  
      jsonGraph.graph.edges.push(link);
    }

  });

  if (dataJSON.linkStyle) {
    const linkStyle = {
      stroke: dataJSON.linkStyle.strokeColor,
      strokeWidth: dataJSON.linkStyle.lineWidth,
      opacity: dataJSON.linkStyle.opacity
    }
    if (jsonGraph.graph.metadata.style?.linkStyles) {
      jsonGraph.graph.metadata.style!.nodeStyles['classicEdge'] = linkStyle;
    } else {
      jsonGraph.graph.metadata.style!['linkStyles'] = {
        classicEdge: linkStyle
      }
    }
  }

  if (dataJSON.metaboliteStyle) {
    const metaboliteStyle = {
      fill: dataJSON.metaboliteStyle.backgroundColor,
      stroke: dataJSON.metaboliteStyle.strokeColor,
      strokeWidth: dataJSON.metaboliteStyle.strokeWidth,
      opacity: dataJSON.metaboliteStyle.opacity,
      height: dataJSON.metaboliteStyle.height,
      width: dataJSON.metaboliteStyle.width,
      shape: 'circle'
    }
    if (jsonGraph.graph.metadata.style?.nodeStyles) {
      jsonGraph.graph.metadata.style!.nodeStyles['metabolite'] = metaboliteStyle;
    } else {
      jsonGraph.graph.metadata.style!['nodeStyles'] = {
        metabolite: metaboliteStyle
      }
    }
  }

  if (dataJSON.reactionStyle) {
    const reactionStyle = {
      fill: dataJSON.reactionStyle.backgroundColor,
      stroke: dataJSON.reactionStyle.strokeColor,
      strokeWidth: dataJSON.reactionStyle.strokeWidth,
      opacity: dataJSON.reactionStyle.opacity,
      height: dataJSON.reactionStyle.height,
      width: dataJSON.reactionStyle.width,
      shape: 'rect'
    }
    if (jsonGraph.graph.metadata.style?.nodeStyles) {
      jsonGraph.graph.metadata.style!.nodeStyles['reaction'] = reactionStyle;
    } else {
      jsonGraph.graph.metadata.style!['nodeStyles'] = {
        reaction: reactionStyle
      }
    }
  }

  return JSON.stringify(jsonGraph);
}